<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProduitController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('/login', [AuthController::class, 'login'])->name('post.login');


Route::get('/register',[AuthController::class, 'register'])->name('register');
Route::post('/register',[AuthController::class, 'createUser'])->name('post.register');


/**
 * home controller routes
 * 
 **/


Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/logout', [HomeController::class, 'logout'])->name('logout');


/**
 * Products controller routes
 * 
 **/

Route::get('/produit/create', [ProduitController::class, 'create'])->name('produit.create');
Route::post('/produit/create', [ProduitController::class, 'store'])->name('post.produit.create');

Route::get('/produit', [ProduitController::class, 'index'])->name('produits');

Route::get('/produit/{id}', [ProduitController::class, 'delete'])->name('produit.delete');
Route::post('/produit/update/{id}', [ProduitController::class, 'update'])->name('post.produit.update');
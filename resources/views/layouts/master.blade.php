<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="{{asset('img/sed.ico')}}" type="image/x-icon">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content />
    <meta name="author" content />
    <title>@yield('title') - Dashbord</title>

    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    <script data-search-pseudo-elements defer src="{{asset('js/all.min.js')}}" ></script>
    <script src="{{asset('js/feather.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}" ></script>
    <link rel="stylesheet" href="{{asset('css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datepicker.min.css')}}">
    <link href="{{asset('css/styles.css')}}" rel="stylesheet" />
    <link href="{{asset('css/loader.css')}}" rel="stylesheet" />
    <!--<link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet'>-->
    @yield('css')
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
    <style>
        .badge{
            min-height: 15px;
            min-width: 15px;
            font-size: 10px;

            border: solid 1px white;
            border-radius: 50%;
            text-align: center;
        }
    </style>
</head>
<body onload="loaderJS()" class="nav-fixed">

<div id="loader"></div>

<div style="display:none;" id="myDiv" class="animate-bottom">
    <nav class="topnav navbar navbar-expand shadow navbar-light sidenav-dark" id="sidenavAccordion">
        <a class="navbar-brand d-none d-sm-block" href="{{route('home')}}">
            <span style="color: white">Tableau De Bord</span>
        </a>
        <ul class="navbar-nav align-items-center ml-auto">
            <!-- pour le user connecté -->
            <li class="nav-item dropdown no-caret mr-3 dropdown-user">
                <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" style="border-radius: 50%; height: 35px; width: 35px;" src="{{url('uploads/avatar/1.PNG')}}"/></a>
                <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up" aria-labelledby="navbarDropdownUserImage">
                    <h6 class="dropdown-header d-flex align-items-center">
                        <img class="dropdown-user-img" src="{{url('uploads/avatar/1.PNG')}}" />
                        <div class="dropdown-user-details">
                            <div class="dropdown-user-details-name">{{\Illuminate\Support\Facades\Auth::user()->prenom}} {{\Illuminate\Support\Facades\Auth::user()->nom}}</div>
                            <div class="dropdown-user-details-email">{{\Illuminate\Support\Facades\Auth::user()->mail}}</div>
                        </div>
                    </h6>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="">
                        <div class="dropdown-item-icon"><i data-feather="settings"></i></div>
                        Mon Compte</a>
                    <a class="dropdown-item" href="{{route('logout')}}"
                    ><div class="dropdown-item-icon"><i data-feather="log-out"></i></div>
                        Déconnexion</a
                    >
                </div>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sidenav shadow-right sidenav-dark">
                <div class="sidenav-menu">
                    <div class="nav accordion" id="accordionSidenav">
                     <!--   <div class="sidenav-menu-heading">Menus</div>-->
                     <!-- debut partie gestion des utilisateurs     -->
                            <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#collapseUsers" aria-expanded="false" aria-controls="collapseUsers">
                                <div class="nav-link-icon"><i data-feather="folder"></i>
                                </div>
                                Gestion des Produits
                                <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseUsers" data-parent="#accordionSidenav">
                                <nav class="sidenav-menu-nested nav"><a class="nav-link" href="{{route('produit.create')}}"><i data-feather="user-plus"></i>&nbsp;Ajouter</a></nav>
                            </div>

                            <div class="collapse" id="collapseUsers" data-parent="#accordionSidenav">
                                <nav class="sidenav-menu-nested nav"><a class="nav-link" href="{{route('produits')}}"><i data-feather="eye"></i>&nbsp;Consulter</a></nav>
                            </div>
                            <div class="collapse" id="collapseUsers" data-parent="#accordionSidenav">
                                <nav class="sidenav-menu-nested nav"><a class="nav-link" href=""><i data-feather="eye-off"></i>&nbsp;Supprimer</a></nav>
                            </div>
                            <!-- fin partie gestion des utilisateurs     -->

                    </div>
                </div>
                <div class="sidenav-footer">
                    <div class="sidenav-footer-content">
                        <div class="sidenav-footer-subtitle">Enregistrer en tant que:</div>
                        <div class="sidenav-footer-title">{{\Illuminate\Support\Facades\Auth::user()->nom}} &nbsp; {{\Illuminate\Support\Facades\Auth::user()->prenom}}
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        

        <div id="layoutSidenav_content">
            <main>
                @yield('content')
            </main>
            <footer class="footer mt-auto footer-light">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 small">Test By Edson Bitjoka&#xA9;{{date('Y')}}</div>
                        <div class="col-md-6 text-md-right small">
                            <a href="#!">Propriété privée</a>
                            &#xB7;
                            <a href="#!">Conditions &amp; d'utilisation</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>

<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>

<script src="{{asset('js/datepicker.js')}}" ></script>
<script src="{{asset('js/datepicker.fr.js')}}" ></script>
<script src="{{asset('js/main.js')}}"></script>

<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/data-table.js')}}"></script>


@include('flashy::message')
@yield('script')
<script>
   var enligne = document.getElementById('enligne');

   var blink = function () {
       if (enligne.style.display == "block") {
           enligne.style.display = "none"
       }else {
           enligne.style.display = "block"
       }
   }
   setInterval(blind,1000)
    var myVar;

    function loaderJS() {
        myVar = setTimeout(showPage, 2000);
    }

    function showPage() {
        document.getElementById("loader").style.display = "none";
        document.getElementById("myDiv").style.display = "block";
    }
    var navbarDropdownAlerts = document.getElementById('navbarDropdownAlerts');
    
    navbarDropdownAlerts.addEventListener('click', function () {
        document.getElementById("notif").innerHTML =""
    })
</script>
</body>
</html>

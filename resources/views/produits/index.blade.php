@extends('layouts.master')
@section('title', "Liste des Produits")
@section('css')
    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" crossorigin="anonymous" />
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.png" />
    <script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.24.1/feather.min.js" crossorigin="anonymous"></script>

@endsection

@section('content')
    <link rel="stylesheet" href="{{asset('js/jquery.dataTables.min.css')}}">


    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">

                <h5 class="page-header-subtitle">Liste des produits disponibles</h5>
                <ol class="breadcrumb mt-4 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Acceuil</a></li>
                    <li class="breadcrumb-item active">Produits</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="card mb-4">
            <div class="card-header">
                <div class="row">
                    <div class="col-10">
                    </div>
                    <div class="col-2">
                    </div>

                </div>
            </div>

            <div class="card-body">
                <div class="datatable table-responsive">
                    <table id="data-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nom du produit</th>
                            <th>Catégorie</th>
                            <th>Prix</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($produits as $produit)
                            <tr>

                                <td><a href="#"><img style="height: 50px; " src="{{url('uploads/produits/'.$produit->image)}}" alt=""/></a></td>
                                <td><a href="#">{{$produit->nom}}</a></td>
                                <td><a href="#">{{$produit->category}}</a></td>
                                <td>{{$produit->prix}} FCFA</td>
                                <td>{{$produit->description}}</td>
                                <td>

                                    <button class="btn btn-datatable btn-icon btn-transparent-dark"  type="button" data-toggle="modal" data-target="#deleteModalCenter{{$produit->id}}">
                                        <i data-feather="trash-2"></i>
                                    </button>
                                    <div class="modal fade" id="deleteModalCenter{{$produit->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle{{$produit->id}}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    voulez vous vraiment supprimer ce produit?
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Non</button>

                                                    <a class="btn btn-primary" href="{{route('produit.delete',$produit->id)}}">Oui</a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button class="btn btn-datatable btn-icon btn-transparent-dark"  type="button" data-toggle="modal" data-target="#updateModalCenter{{$produit->id}}">
                                        <i data-feather="more-vertical"></i>
                                    </button>
                                    <div class="modal fade" id="updateModalCenter{{$produit->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle{{$produit->id}}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    Modifier le Produit
                                                    <form method="post" autocomplete="off" action="{{route('post.produit.update',$produit->id)}}" enctype="multipart/form-data">
                                                        {{csrf_field()}}

                                                        <div class="form-group">
                                                            <label for="">Nom du produit</label>
                                                            <input type="text" name="nom" value="{{$produit->nom}}" class="form-control" placeholder="Entrez le nom du produit">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">catégorie</label>
                                                            <input value="{{$produit->category}}" name="category" placeholder="Entrez la catégorie du produit" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Description</label>
                                                            <textarea name="description" type="text" class="form-control">{{$produit->description}} </textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Prix (en chiffres)</label>
                                                            <input value="{{$produit->prix}}"  name="prix" type="text" class="form-control" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="avatar">Image du produit</label>
                                                            <input  type="file" name="avatar" class="form-control" placeholder="Entrez le nom du produit">
                                                        </div>



                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-outline-primary rounded-pill">
                                                                <span>Modifier le produit</span>
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </td>
                            </tr>
                        @endforeach


                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#tableuser').DataTable();
        } );
    </script>

@endsection


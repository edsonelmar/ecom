@extends('layouts.master')
@section('title', "Create product")
@section('css')
    <style>
        .form-control{
            border-radius: 20px;
        }
    </style>
@endsection

@section('content')
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">

                <h5 class="page-header-subtitle">creattion d'un produit</h5>
                <ol class="breadcrumb mt-4 mb-0">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Acceuil</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('produits')}}">Produits</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="card">

            <div class="card-body">
                <div class="row">
                    <div class="col-md-8 offset-2">
                        <form method="post" autocomplete="off" action="{{route('post.produit.create')}}" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label for="">Nom du produit</label>
                                <input required="required" type="text" name="nom" class="form-control" placeholder="Entrez le nom du produit">
                            </div>
                            <div class="form-group">
                                <label for="">catégorie</label>
                                <input required="required" name="category" placeholder="Entrez la catégorie du produit" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <textarea required="required" name="description" type="text" class="form-control"> </textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Prix (en chiffres)</label>
                                <input required="required" name="prix" type="text" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="avatar">Image du produit</label>
                                <input required="required" type="file" name="avatar" class="form-control" placeholder="Entrez le nom du produit">
                            </div>



                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-primary rounded-pill">
                                    <span>AJouter un produit dans la boutique</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

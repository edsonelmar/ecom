<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Register</title>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css">
</head>
<body>


<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

@include('flashy::message')

  <section class="vh-100 bg-image">
  <div class="mask d-flex align-items-center h-100 gradient-custom-3">
    <div class="container h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-12 col-md-9 col-lg-7 col-xl-6">
          <div class="card" style="border-radius: 15px;">
            <div class="card-body p-5">
              <h2 class="text-uppercase text-center mb-5">Inscription</h2>

              <form method="POST" action="{{ route('post.register') }}">
                @csrf

                <div class="form-outline mb-4">
                  <input type="text" name="nom" class="form-control form-control-lg" />
                  <label class="form-label" for="form3Example1cg">Nom</label>
                </div>
                <div class="form-outline mb-4">
                  <input type="text" name="prenom" class="form-control form-control-lg" />
                  <label class="form-label" for="prenom">Prenom</label>
                </div>

                <div class="form-outline mb-4">
                  <input type="email" name="email" class="form-control form-control-lg" />
                  <label class="form-label" for="email">Email</label>
                </div>

                <div class="form-outline mb-4">
                  <input type="password" name="mdp" class="form-control form-control-lg" />
                  <label class="form-label" for="identifiant">Mot de passe</label>
                </div>

                <div class="d-flex justify-content-center">
                  <button type="submit" class="btn btn-primary btn-block btn-lg gradient-custom-4 text-body">Inscription</button>
                </div>

              </form>
              <div class="form-group">
                  <p><a href="{{route('login')}}">cliquez ici pour vous connecter</a></p>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</body>
</html>
@extends('layouts.master')

@section('content')
    <div class="row">
        <h1 class="col-12 text-center">TEST POUR LA CREATION DES PRODUITS</h1>
        <hr>
    </div>
    <div class="row">
        <br>
        <div class="col-md-6">
            <div class="card">
              <div class="card-body">
                <p class="text-center">Nombre d'utilisateurs</p>
                <br>
                <h1 class="text-center">{{count($users)}}</h1>
                <p class="text-center"><a href="">Voir tous les utilisateurs</a></p>
              </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
              <div class="card-body">
                <p class="text-center">Nombre de produits</p>
                <br>
                <h1 class="text-center">{{count($produits)}}</h1>
                <p class="text-center"><a href="{{route('produits')}}">Voir tous les produits</a></p>
              </div>
            </div>
        </div>
    </div>
@endsection
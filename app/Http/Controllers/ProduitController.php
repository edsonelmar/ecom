<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MercurySeries\Flashy\Flashy;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ProduitController extends Controller
{

    public function  __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $produits = Produit::Orderby('created_at','DESC')->get();
           
     return view('produits.index', compact('produits'));
    }
    // public function deleted(){
    //     $bons = Bon::where('status', 2)
    //         ->orderby('date_bon','DESC')->get();
    //     return view('bons.deleted', compact('bons'));
    // }
    // public function used(){
    //     $bons = Bon::where('status', 1)
    //         ->orderby('date_bon','DESC')->get();
    //     return view('bons.used', compact('bons'));
    // }
    // public function show($id){
    //     $bons = Bon::where('identifiant', $id)->get();
    //     $bonss = [];
    //     foreach ($bons as $b)
    //     {
    //         $bonss[] = $b;
    //     }
    //     $bon =$bonss[0];
    //     //dd($bon);
    //     return view('bons.show', compact('bon'));
    // }

//create product get page
    public function create(){
        
        return view("produits.create");
    }

    public function delete($id){
            Produit::where('id',$id)->delete();
            Flashy::success("le bon a été supprimé avec success");
            return redirect()->back();

    }
    public function update(Request $request, $id){

        if ($request->hasFile('avatar')) {

            //uploader une image
            $file = $request->file('avatar');
            $files = $request->file('avatar')->getClientOriginalName();
            $imageFileName = time().'_'.$files;
            $path = public_path('/uploads/produits');
            $file->move($path, $imageFileName);

            Produit::where('id',$id)->update([
                'nom' => $request->nom,
                'category' =>$request->category,
                'image' =>$imageFileName,
                'description' =>$request->description,
                'prix' =>$request->prix,
                'updated_at'=>Carbon::now(),
            ]);
            Flashy::success("le produit a été modifié avec success");
            return redirect()->back();
        }

        Produit::where('id',$id)->update([
                'nom' => $request->nom,
                'category' =>$request->category,
                'description' =>$request->description,
                'prix' =>$request->prix,
                'updated_at'=>Carbon::now(),
            ]);

        
        Flashy::success("le produit a été modifié avec success");
        return redirect()->back();
    }


//create product store action
    public function store(Request $request){

        $imageFileName ='';

        if ($request->hasFile('avatar')) {

            //uploader une image
            $file = $request->file('avatar');
            $files = $request->file('avatar')->getClientOriginalName();
            $imageFileName = time().'_'.$files;
            $path = public_path('/uploads/produits');
            $file->move($path, $imageFileName);
        }
        $slug = Str::slug($request->nom.'&'.$request->category.'-'.time());

        Produit::create([
            'nom' => $request->nom,
            'slug' => $slug,
            'category' =>$request->category,
            'image' =>$imageFileName,
            'description' =>$request->description,
            'prix' =>$request->prix,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);

        Flashy::success("le produit a été crée avec succes");
        return redirect()->back();
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Produit;

use MercurySeries\Flashy\Flashy;

class HomeController extends Controller
{
    public function  __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        $users = User::all();
        $produits = Produit::all();
        return view('home', compact('users', 'produits'));
    }

    public function logout(){  
        auth()->logout();
        flashy()->success('Deconnexion reussit avec success.');
        return redirect()->route('login');
    }
}

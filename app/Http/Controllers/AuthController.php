<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use MercurySeries\Flashy\Flashy;
use Illuminate\Support\Str;
use App\Models\User;

class AuthController extends Controller
{
    public function  __construct(){
        $this->middleware('guest');
    }


    public function index(){
        return view('auth.login');
    }
    public function register(){
        return view('auth.register');
    }

    public function login(Request $request){

        //ca retourne true si les informations sont correctent et false sinon
        if(!Auth::attempt(['email' => $request->email,'password' => $request->mdp]))
        {

            Flashy::error('Votre Identifiant où mot de passe est incorrect');
            return redirect()->back();

        }else{

            Flashy::success('Bienvenu Mr/Mme: '.Auth::user()->prenom.' '.Auth::user()->nom);
            return redirect()->route('home');
        }
    }

    public function logout(){
        auth()->logout();
        flashy()->success('Deconnexion reussit avec success.');
        return redirect()->route('login');
    }

    //fonction pour creer un utilisateur
    public function createUser(Request $request)
    {
        if (empty($request->nom) || empty($request->prenom) || empty($request->email)|| empty($request->mdp)) {
            Flashy::error(" tous les champs doivent etre rempli");
            return redirect()->back();
        }
         //dd("je suis ici");
        $slug = Str::slug($request->nom.'&'.$request->prenom.'-'.time());
        $user = new User;
        $user->nom = $request->nom;
        $user->prenom = $request->prenom;
        $user->email = $request->email;
        $user->slug = $slug;
        $user->password = bcrypt($request->mdp);
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        if ($user->save()) {
            auth()->loginUsingId($user->id);
            Flashy::error("L'utilisateur a été enregistrer avec success");
            return redirect()->route('home');
        }

        Flashy::error("L'utilisateur n'a pas été enregistrer avec success");
        return redirect()->back();
       
    }
}
